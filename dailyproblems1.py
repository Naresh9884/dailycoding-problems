Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
def pairsum(numlist,requiredsum):
    dict = {}
    for i in numlist:
        if requiredsum + i not in dict:
            dict[requiredsum + i] = 0
        if i in dict:
            print(i,requiredsum+i)
            return False
        return True
arr = [10,15,3,7]
print(pairsum(arr,17))